package ru.bazhenov.funboxtest.di;


import android.content.res.AssetManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AssetModule {

    private final AssetManager assetManager;

    public AssetModule(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    @Provides
    @Singleton
    public AssetManager provideAssetManager() {
        return assetManager;
    }
}
