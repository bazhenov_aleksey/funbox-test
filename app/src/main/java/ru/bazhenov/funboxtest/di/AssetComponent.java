package ru.bazhenov.funboxtest.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.bazhenov.funboxtest.data_providers.CSVFileParser;

@Component(modules = {AssetModule.class})
@Singleton
public interface AssetComponent {
    void inject(CSVFileParser parser);
}
