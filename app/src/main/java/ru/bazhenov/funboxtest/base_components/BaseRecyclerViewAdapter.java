package ru.bazhenov.funboxtest.base_components;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, VH extends BaseViewHolder> extends RecyclerView.Adapter<VH> {

    protected List<T> items;
    protected Context context;

    public BaseRecyclerViewAdapter(Context context, List<T> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public final VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return onCreateRecyclerViewHolder(parent, viewType);
    }

    @Override
    public final void onBindViewHolder(VH holder, int position) {
        if (items != null) {
            holder.performBind(items.get(position), position);
        }
        onBindRecyclerViewHolder(holder, position);
    }

    public abstract VH onCreateRecyclerViewHolder(ViewGroup parent, int viewType);

    public abstract void onBindRecyclerViewHolder(VH holder, int position);

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateData(List<T> newItems) {
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    public void updateItem(int position, T updated) {
        items.set(position, updated);
        notifyItemChanged(position);
    }

    public void deleteItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    public void insertStoreItem(int position, T item) {
        items.add(item);
        notifyItemInserted(position);
    }
}
