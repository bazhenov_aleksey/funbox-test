package ru.bazhenov.funboxtest.base_components;

import android.support.v4.app.Fragment;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

public abstract class BaseFragment extends Fragment {

    protected abstract void setupAdapter(List<StoreItem> items);
    protected abstract RealmResults<StoreItem> getList();
    protected abstract BaseRecyclerViewAdapter getAdapter();

    protected Realm realm;
    private RealmResults<StoreItem> storeItems;

    @Override
    public void onStart() {
        super.onStart();

        realm = Realm.getDefaultInstance();
        storeItems = getList();

        setupAdapter(getListFromResults(storeItems));

        storeItems.addChangeListener((storeItems, changeSet) -> {
            if (changeSet.getChanges().length > 0) {
                int position = changeSet.getChanges()[0];
                updateStoreItem(position, storeItems.get(position));
            } else if (changeSet.getDeletions().length > 0) {
                int position = changeSet.getDeletions()[0];
                deleteStoreItem(position);
            } else if (changeSet.getInsertions().length > 0) {
                int position = changeSet.getInsertions()[0];
                insertStoreItem(position, storeItems.get(position));
            }
        });
    }

    private void updateStoreItem(int position, StoreItem storeItem) {
        getAdapter().updateItem(position, storeItem);
    }

    private void deleteStoreItem(int position) {
        getAdapter().deleteItem(position);
    }

    private void insertStoreItem(int position, StoreItem storeItem) {
        getAdapter().insertStoreItem(position, storeItem);
    }

    private List<StoreItem> getListFromResults(RealmResults<StoreItem> storeItems) {
        return realm.copyFromRealm(storeItems);
    }

    @Override
    public void onStop() {
        super.onStop();
        storeItems.removeAllChangeListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
