package ru.bazhenov.funboxtest.base_components;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    protected T item;
    protected int position;

    protected BaseViewHolder(View itemView) {
        super(itemView);
    }

    protected final void performBind(T item, int position) {
        this.item = item;
        this.position = position;
    }

    public T getItem() {
        return item;
    }
}