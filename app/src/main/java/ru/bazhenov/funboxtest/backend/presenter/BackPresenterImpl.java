package ru.bazhenov.funboxtest.backend.presenter;

import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.backend.view.BackView;
import ru.bazhenov.funboxtest.backend.model.BackModel;
import ru.bazhenov.funboxtest.backend.model.BackModelImpl;
import rx.Subscriber;

public class BackPresenterImpl implements BackPresenter {

    private BackView view;
    private BackModel model;

    private RealmResults<StoreItem> storeItems;

    public BackPresenterImpl(BackView view) {
        this.view = view;
        model = new BackModelImpl();
    }

    @Override
    public void onCreateBackView() {
        model.getStoreItems().subscribe(new Subscriber<RealmResults<StoreItem>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                view.showNotification(e.getMessage());
            }

            @Override
            public void onNext(RealmResults<StoreItem> items) {
                storeItems = items;
            }
        });

    }

    @Override
    public RealmResults<StoreItem> getStoreItems() {
        return storeItems;
    }
}
