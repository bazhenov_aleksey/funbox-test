package ru.bazhenov.funboxtest.backend.model;

import io.realm.Realm;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

public interface StoreItemModel {

    StoreItem getStoreItem(long itemId);

    void editStoreItem(StoreItem item, Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError);

    void createStoreItem(StoreItem item, Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError);

    void closeRealm();
}
