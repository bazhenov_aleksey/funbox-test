package ru.bazhenov.funboxtest.backend.presenter;

import io.realm.Realm;
import ru.bazhenov.funboxtest.backend.model.StoreItemModel;
import ru.bazhenov.funboxtest.backend.model.StoreItemModelImpl;
import ru.bazhenov.funboxtest.backend.view.StoreItemView;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

import static ru.bazhenov.funboxtest.Constants.DEFAULT_ITEM_ID;

public class StoreItemPresenterImpl implements StoreItemPresenter {

    private StoreItemView view;
    private StoreItemModel model;

    public StoreItemPresenterImpl(StoreItemView view) {
        this.view = view;
        model = new StoreItemModelImpl();
    }

    @Override
    public void onCreateView() {
        if (editMode()) {
            StoreItem item = model.getStoreItem(view.getItemId());
            view.setupStoreItemViews(item);
        }
    }

    @Override
    public void onSaveClick() {
        if (editMode()) {
            updateStoreItem();
        } else {
            createStoreItem();
        }
    }

    private void updateStoreItem() {
        model.editStoreItem(view.getItem(),
                onSuccess,
                onError);
    }

    private void createStoreItem() {
        model.createStoreItem(view.getItem(),
                onSuccess,
                onError);
    }

    private Realm.Transaction.OnSuccess onSuccess = () -> view.showNotification("Товар сохранен");

    private Realm.Transaction.OnError onError = error -> view.showNotification(error.getMessage());

    @Override
    public void onDestroyView() {
        model.closeRealm();
    }

    private boolean editMode() {
        return view.getItemId() != DEFAULT_ITEM_ID;
    }
}
