package ru.bazhenov.funboxtest.backend.presenter;

public interface StoreItemPresenter {

    void onCreateView();

    void onSaveClick();

    void onDestroyView();
}
