package ru.bazhenov.funboxtest.backend.view;

import ru.bazhenov.funboxtest.data_providers.StoreItem;

public interface StoreItemView {

    long getItemId();

    void setupStoreItemViews(StoreItem item);

    StoreItem getItem();

    void showNotification(String message);
}
