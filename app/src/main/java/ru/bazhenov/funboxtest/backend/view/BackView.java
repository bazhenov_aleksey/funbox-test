package ru.bazhenov.funboxtest.backend.view;

public interface BackView {

    void showNotification(String message);
}
