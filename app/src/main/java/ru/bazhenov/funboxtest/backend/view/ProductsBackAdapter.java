package ru.bazhenov.funboxtest.backend.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bazhenov.funboxtest.R;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.base_components.BaseRecyclerViewAdapter;
import ru.bazhenov.funboxtest.base_components.BaseViewHolder;

class ProductsBackAdapter extends BaseRecyclerViewAdapter<StoreItem, ProductsBackAdapter.ViewHolder> {

    public ProductsBackAdapter(Context context, List<StoreItem> storeItems) {
        super(context, storeItems);
    }

    @Override
    public ViewHolder onCreateRecyclerViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_back, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindRecyclerViewHolder(ViewHolder holder, int position) {
        final StoreItem item = holder.getItem();

        holder.productName.setText(item.getName());
        holder.amount.setText(String.valueOf(item.getAmount()));
        holder.cardView.setOnClickListener(view -> StoreItemActivity.start(context, item.getId()));
    }

    public static class ViewHolder extends BaseViewHolder<StoreItem> {

        @BindView(R.id.card_layout)
        CardView cardView;
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.amount)
        TextView amount;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
