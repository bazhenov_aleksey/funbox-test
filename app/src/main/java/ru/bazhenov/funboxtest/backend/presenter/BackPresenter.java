package ru.bazhenov.funboxtest.backend.presenter;

import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

public interface BackPresenter {

    void onCreateBackView();

    RealmResults<StoreItem> getStoreItems();
}
