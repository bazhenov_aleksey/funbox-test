package ru.bazhenov.funboxtest.backend.model;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import rx.Observable;

public class BackModelImpl implements BackModel {

    @Override
    public Observable<RealmResults<StoreItem>> getStoreItems() {
        return Realm.getDefaultInstance().where(StoreItem.class)
                .findAll()
                .asObservable();
    }
}
