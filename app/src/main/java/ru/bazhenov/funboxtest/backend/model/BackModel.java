package ru.bazhenov.funboxtest.backend.model;

import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import rx.Observable;

public interface BackModel {

    Observable<RealmResults<StoreItem>> getStoreItems();

}
