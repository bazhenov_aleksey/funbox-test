package ru.bazhenov.funboxtest.backend.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import ru.bazhenov.funboxtest.R;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.backend.presenter.BackPresenter;
import ru.bazhenov.funboxtest.backend.presenter.BackPresenterImpl;
import ru.bazhenov.funboxtest.base_components.BaseFragment;
import ru.bazhenov.funboxtest.base_components.BaseRecyclerViewAdapter;

public class BackendFragment extends BaseFragment implements BackView{

    public static final String TAG = "BackendFragment";

    @BindView(R.id.products)
    RecyclerView recyclerView;

    private ProductsBackAdapter adapter;

    private BackPresenter presenter;

    public static BackendFragment newInstance() {
        return new BackendFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backend, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        presenter = new BackPresenterImpl(this);
        presenter.onCreateBackView();
        return view;
    }

    @Override
    protected void setupAdapter(List<StoreItem> items) {
        if (adapter == null) {
            adapter = new ProductsBackAdapter(getContext(), items);
        } else {
            adapter.updateData(items);
        }
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected RealmResults<StoreItem> getList() {
        return realm.where(StoreItem.class).findAll();
    }

    @Override
    protected BaseRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_add:
                StoreItemActivity.start(getContext());
                return false;
            default:
                break;
        }
        return false;
    }

    @Override
    public void showNotification(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
