package ru.bazhenov.funboxtest.backend.model;

import android.support.annotation.NonNull;

import io.realm.Realm;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

import static ru.bazhenov.funboxtest.Constants.DELAY_UPDATE_ITEM;

public class StoreItemModelImpl implements StoreItemModel {

    private Realm realm;

    public StoreItemModelImpl() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void editStoreItem(StoreItem item,
                              Realm.Transaction.OnSuccess onSuccess,
                              Realm.Transaction.OnError onError) {
        realm.executeTransactionAsync(realm -> {
                    waitFiveSeconds();
                    StoreItem existingItem = realm.where(StoreItem.class)
                            .equalTo("id", item.getId())
                            .findFirst();
                    existingItem.setName(item.getName());
                    existingItem.setPrice(item.getPrice());
                    existingItem.setAmount(item.getAmount());
                },
                onSuccess,
                onError);
    }

    @Override
    public void createStoreItem(StoreItem item, Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError) {
        realm.executeTransactionAsync(realm -> {
                    long maxId = realm.where(StoreItem.class).max("id").longValue();
                    StoreItem newItem = createNewStoreItem(maxId, item);
                    realm.insert(newItem);
                },
                onSuccess,
                onError);
    }

    @Override
    public void closeRealm() {
        realm.close();
    }

    @NonNull
    private StoreItem createNewStoreItem(long maxId, StoreItem item) {
        StoreItem newItem = new StoreItem();
        newItem.setId(maxId + 1);
        newItem.setName(item.getName());
        newItem.setPrice(item.getPrice());
        newItem.setAmount(item.getAmount());
        return newItem;
    }

    @Override
    public StoreItem getStoreItem(long itemId) {
        return realm.where(StoreItem.class)
                .equalTo("id", itemId)
                .findFirst();
    }

    private void waitFiveSeconds() {
        try {
            Thread.sleep(DELAY_UPDATE_ITEM);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
