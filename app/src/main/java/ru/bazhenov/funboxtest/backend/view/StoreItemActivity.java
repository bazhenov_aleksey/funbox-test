package ru.bazhenov.funboxtest.backend.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bazhenov.funboxtest.R;
import ru.bazhenov.funboxtest.backend.presenter.StoreItemPresenter;
import ru.bazhenov.funboxtest.backend.presenter.StoreItemPresenterImpl;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

import static ru.bazhenov.funboxtest.Constants.DEFAULT_ITEM_ID;

public class StoreItemActivity extends AppCompatActivity implements StoreItemView {

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.amount)
    EditText amount;

    private static final String EXTRA_STORE_ITEM_ID = "EXTRA_STORE_ITEM_ID";

    private StoreItemPresenter presenter;

    public static void start(Context context) {
        Intent intent = new Intent(context, StoreItemActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, long itemId) {
        Intent intent = new Intent(context, StoreItemActivity.class);
        intent.putExtra(EXTRA_STORE_ITEM_ID, itemId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new StoreItemPresenterImpl(this);
        presenter.onCreateView();
    }

    @Override
    public long getItemId() {
        return getIntent().getLongExtra(EXTRA_STORE_ITEM_ID, DEFAULT_ITEM_ID);
    }

    @Override
    public void setupStoreItemViews(StoreItem item) {
        name.setText(item.getName());
        price.setText(String.valueOf(item.getPrice()));
        amount.setText(String.valueOf(item.getAmount()));
    }

    @Override
    public StoreItem getItem() {
        StoreItem item = new StoreItem();
        item.setId(getItemId());
        item.setName(name.getText().toString());
        item.setPrice(Double.parseDouble(price.getText().toString()));
        item.setAmount(Integer.parseInt(amount.getText().toString()));
        return item;
    }

    @Override
    public void showNotification(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_action_save:
                presenter.onSaveClick();
                return false;
            default:
                break;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroyView();
    }
}
