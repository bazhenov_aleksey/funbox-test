package ru.bazhenov.funboxtest;

public class Constants {

    public static final String CSV_FILE_NAME = "data.csv";

    public static final int PRODUCT_NAME_COLUMN = 0;
    public static final int PRODUCT_PRICE_COLUMN = 1;
    public static final int PRODUCT_AMOUNT_COLUMN = 2;

    public static final int DELAY_BUY_ITEM = 3000; // mills
    public static final int DELAY_UPDATE_ITEM = 5000; // mills

    public static final long DEFAULT_ITEM_ID = -1;

}
