package ru.bazhenov.funboxtest.data_providers;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import static ru.bazhenov.funboxtest.Constants.PRODUCT_AMOUNT_COLUMN;
import static ru.bazhenov.funboxtest.Constants.PRODUCT_NAME_COLUMN;
import static ru.bazhenov.funboxtest.Constants.PRODUCT_PRICE_COLUMN;

public class StoreItem extends RealmObject {

    @PrimaryKey
    private long id;
    private String name;
    private double price;
    private int amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public static StoreItem getInstanceFromLine(long id, String line) {
        String[] rowData = line.replaceAll("\"", "").split(", ");
        String name = rowData[PRODUCT_NAME_COLUMN];
        String price = rowData[PRODUCT_PRICE_COLUMN];
        String amount = rowData[PRODUCT_AMOUNT_COLUMN];

        StoreItem storeItem = new StoreItem();
        storeItem.setId(id);
        storeItem.setName(name);
        storeItem.setPrice(Double.parseDouble(price));
        storeItem.setAmount(Integer.parseInt(amount));
        return storeItem;
    }
}
