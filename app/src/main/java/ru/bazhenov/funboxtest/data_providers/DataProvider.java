package ru.bazhenov.funboxtest.data_providers;

import java.util.List;

import rx.Observable;

public interface DataProvider {

    Observable<List<StoreItem>> getData();
}
