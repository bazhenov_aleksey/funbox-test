package ru.bazhenov.funboxtest.data_providers;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import ru.bazhenov.funboxtest.StoreApp;
import rx.Observable;

public class CSVFileParser implements DataProvider {

    @Inject
    AssetManager assetManager;

    private String fileName;

    public CSVFileParser init(final String fileName) {
        StoreApp.getAssetComponent().inject(this);
        this.fileName = fileName;
        return this;
    }

    @Override
    public Observable<List<StoreItem>> getData() {
        if (fileName == null) {
            throw new RuntimeException("CSVFileParser not initialized. Call init() method");
        }

        return Observable.create(subscriber -> {
            List<StoreItem> list = new ArrayList<>();
            InputStream inputStream = null;
            try {
                inputStream = assetManager.open(fileName);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                long i = 0;
                while ((line = reader.readLine()) != null) {
                    StoreItem item = StoreItem.getInstanceFromLine(i, line);
                    list.add(item);
                    i++;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            CSVFileParser.this.putProductsToDb(list);

            subscriber.onNext(list);
        });
    }

    private void putProductsToDb(final List<StoreItem> storeItems) {
        Realm.getDefaultInstance().executeTransaction(realm -> realm.insertOrUpdate(storeItems));
    }


}
