package ru.bazhenov.funboxtest.data_providers;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;

public class DBProvider implements DataProvider {

    @Override
    public Observable<List<StoreItem>> getData() {
        final Realm realm = Realm.getDefaultInstance();
        RealmResults<StoreItem> results = realm.where(StoreItem.class).findAll();
        List<StoreItem> storeItems = realm.copyFromRealm(results);
        return Observable.just(storeItems);
    }
}
