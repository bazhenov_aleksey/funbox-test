package ru.bazhenov.funboxtest.main.view;

public interface StoreView {
    void showStoreItems();

    void showToast(String message);
}
