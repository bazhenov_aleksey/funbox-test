package ru.bazhenov.funboxtest.main.view;

import android.support.v4.app.Fragment;

import ru.bazhenov.funboxtest.backend.view.BackendFragment;
import ru.bazhenov.funboxtest.front.view.FrontFragment;

public class NavigationMenuFactory {

    public static Fragment getFragment(String tag) {
        Fragment fragment;
        switch (tag) {
            case FrontFragment.TAG:
                fragment = FrontFragment.newInstance();
                break;
            case BackendFragment.TAG:
                fragment = BackendFragment.newInstance();
                break;
            default:
                throw new RuntimeException("NavigationMenuFactory - bottom menu has only 2 items");
        }
        return fragment;
    }
}
