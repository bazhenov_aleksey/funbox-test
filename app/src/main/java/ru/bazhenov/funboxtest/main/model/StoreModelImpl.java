package ru.bazhenov.funboxtest.main.model;

import java.util.List;

import io.realm.Realm;
import ru.bazhenov.funboxtest.data_providers.CSVFileParser;
import ru.bazhenov.funboxtest.data_providers.DBProvider;
import ru.bazhenov.funboxtest.data_providers.DataProvider;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static ru.bazhenov.funboxtest.Constants.CSV_FILE_NAME;

public class StoreModelImpl implements StoreModel {

    @Override
    public Observable<List<StoreItem>> loadGoods() {
        DataProvider dataProvider;
        if (isCsvLoaded()) {
            dataProvider = new DBProvider();
        } else {
            dataProvider = new CSVFileParser().init(CSV_FILE_NAME);
        }
        return dataProvider.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private boolean isCsvLoaded() {
        return Realm.getDefaultInstance().where(StoreItem.class).count() != 0;
    }
}
