package ru.bazhenov.funboxtest.main.model;

import java.util.List;

import ru.bazhenov.funboxtest.data_providers.StoreItem;
import rx.Observable;

public interface StoreModel {
    Observable<List<StoreItem>> loadGoods();
}
