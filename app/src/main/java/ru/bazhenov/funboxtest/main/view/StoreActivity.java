package ru.bazhenov.funboxtest.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bazhenov.funboxtest.R;
import ru.bazhenov.funboxtest.backend.view.BackendFragment;
import ru.bazhenov.funboxtest.front.view.FrontFragment;
import ru.bazhenov.funboxtest.main.presenter.StorePresenter;
import ru.bazhenov.funboxtest.main.presenter.StorePresenterImpl;

public class StoreActivity extends AppCompatActivity implements StoreView {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        actionBar = getSupportActionBar();

        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        StorePresenter presenter = new StorePresenterImpl(this);
        presenter.onCreateView();
    }

    @Override
    public void showStoreItems() {
        progressBar.setVisibility(View.GONE);
        bottomNavigation.setSelectedItemId(R.id.menu_front);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(StoreActivity.this, message, Toast.LENGTH_LONG).show();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_front:
                    actionBar.setTitle(R.string.menu_title_store_front);
                    setupFragment(FrontFragment.TAG);
                    return true;
                case R.id.menu_back:
                    actionBar.setTitle(R.string.menu_title_back_end);
                    setupFragment(BackendFragment.TAG);
                    return true;
                default:
                    return true;
            }
        }
    };

    private void setupFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment == null) {
            fragment = NavigationMenuFactory.getFragment(tag);
            fragmentTransaction.add(R.id.container, fragment, tag);
        } else {
            fragmentTransaction.attach(fragment);
        }

        Fragment currentFragment = fragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.detach(currentFragment);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragment);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }
}
