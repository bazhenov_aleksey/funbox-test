package ru.bazhenov.funboxtest.main.presenter;

import java.util.List;

import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.main.view.StoreView;
import ru.bazhenov.funboxtest.main.model.StoreModel;
import ru.bazhenov.funboxtest.main.model.StoreModelImpl;
import rx.Subscriber;

public class StorePresenterImpl implements StorePresenter {

    private StoreView view;
    private StoreModel model;

    public StorePresenterImpl(StoreView view) {
        this.view = view;
        model = new StoreModelImpl();
    }

    @Override
    public void onCreateView() {
        model.loadGoods()
                .subscribe(new Subscriber<List<StoreItem>>() {
                    @Override
                    public void onNext(List<StoreItem> storeItems) {
                        view.showStoreItems();
                    }

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showToast(e.getMessage());
                    }
                });
    }
}