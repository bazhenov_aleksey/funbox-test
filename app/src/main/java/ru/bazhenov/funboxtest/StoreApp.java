package ru.bazhenov.funboxtest;

import android.app.Application;

import io.realm.Realm;
import ru.bazhenov.funboxtest.di.AssetComponent;
import ru.bazhenov.funboxtest.di.AssetModule;
import ru.bazhenov.funboxtest.di.DaggerAssetComponent;

public class StoreApp extends Application {

    private static AssetComponent assetComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        assetComponent = buildComponent();
    }

    public static AssetComponent getAssetComponent() {
        return assetComponent;
    }

    private AssetComponent buildComponent() {
        return DaggerAssetComponent.builder()
                .assetManagerModule(new AssetModule(getAssets()))
                .build();
    }

}
