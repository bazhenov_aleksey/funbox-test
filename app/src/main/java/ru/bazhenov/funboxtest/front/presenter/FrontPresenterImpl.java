package ru.bazhenov.funboxtest.front.presenter;

import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.front.model.FrontModel;
import ru.bazhenov.funboxtest.front.model.FrontModelImpl;
import ru.bazhenov.funboxtest.front.view.FrontView;
import rx.Subscriber;

public class FrontPresenterImpl implements FrontPresenter {

    private FrontView view;
    private FrontModel model;

    private RealmResults<StoreItem> storeItems;

    public FrontPresenterImpl(FrontView view) {
        this.view = view;
        model = new FrontModelImpl();
    }

    @Override
    public void onCreateFrontView() {
        model.getStoreItems().subscribe(new Subscriber<RealmResults<StoreItem>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                view.showNotification(e.getMessage());
            }

            @Override
            public void onNext(RealmResults<StoreItem> items) {
                storeItems = items;
            }
        });

    }

    @Override
    public RealmResults<StoreItem> getStoreItems() {
        return storeItems;
    }

    @Override
    public void onBuyClick(long itemId) {
        model.updateStoreItem(
                itemId,
                error -> view.showNotification(error.getMessage())
        );
    }
}
