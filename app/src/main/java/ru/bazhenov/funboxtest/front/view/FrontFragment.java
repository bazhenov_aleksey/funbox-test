package ru.bazhenov.funboxtest.front.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import ru.bazhenov.funboxtest.R;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.base_components.BaseFragment;
import ru.bazhenov.funboxtest.base_components.BaseRecyclerViewAdapter;
import ru.bazhenov.funboxtest.front.presenter.FrontPresenter;
import ru.bazhenov.funboxtest.front.presenter.FrontPresenterImpl;

public class FrontFragment extends BaseFragment implements FrontView {

    public static final String TAG = "FrontFragment";

    @BindView(R.id.scrollview)
    DiscreteScrollView scrollView;

    private ProductsFrontAdapter adapter;

    private FrontPresenter presenter;

    public static FrontFragment newInstance() {
        return new FrontFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store, container, false);
        ButterKnife.bind(this, view);

        scrollView.setOrientation(Orientation.HORIZONTAL);
        scrollView.setItemTransitionTimeMillis(150);
        scrollView.setItemTransformer(new ScaleTransformer.Builder().build());

        presenter = new FrontPresenterImpl(this);
        presenter.onCreateFrontView();
        return view;
    }

    @Override
    protected void setupAdapter(List<StoreItem> items) {
        if (adapter == null) {
            adapter = new ProductsFrontAdapter(getContext(), items, listener);
        } else {
            adapter.updateData(items);
        }
        scrollView.setAdapter(adapter);
    }

    @Override
    protected RealmResults<StoreItem> getList() {
        return presenter.getStoreItems();
    }

    @Override
    protected BaseRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void showNotification(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private BuyClickListener listener = new BuyClickListener() {
        @Override
        public void onClick(long itemId) {
            presenter.onBuyClick(itemId);
        }
    };
}
