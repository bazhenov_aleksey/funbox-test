package ru.bazhenov.funboxtest.front.presenter;

import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;

public interface FrontPresenter {

    void onCreateFrontView();

    RealmResults<StoreItem> getStoreItems();

    void onBuyClick(long itemId);
}
