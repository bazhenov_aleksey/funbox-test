package ru.bazhenov.funboxtest.front.view;

public interface FrontView {

    void showNotification(String message);

}
