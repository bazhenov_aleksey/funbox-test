package ru.bazhenov.funboxtest.front.view;

public interface BuyClickListener {

    void onClick(long itemId);
}
