package ru.bazhenov.funboxtest.front.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.bazhenov.funboxtest.R;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import ru.bazhenov.funboxtest.base_components.BaseRecyclerViewAdapter;
import ru.bazhenov.funboxtest.base_components.BaseViewHolder;

public class ProductsFrontAdapter extends BaseRecyclerViewAdapter<StoreItem, ProductsFrontAdapter.ViewHolder> {

    private BuyClickListener buyClickListener;

    public ProductsFrontAdapter(Context context, List<StoreItem> storeItems, BuyClickListener buyClickListener) {
        super(context, storeItems);
        this.buyClickListener = buyClickListener;
    }

    @Override
    public ViewHolder onCreateRecyclerViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_front, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindRecyclerViewHolder(ViewHolder holder, int position) {
        final StoreItem item = holder.getItem();
        holder.productName.setText(item.getName());
        holder.price.setText(String.format(Locale.getDefault(), "%.2f р.", item.getPrice()));
        holder.amount.setText(String.format(Locale.getDefault(), "Осталось %d шт.", item.getAmount()));
        holder.progress.setVisibility(View.GONE);
        holder.buy.setEnabled(true);

        holder.buy.setOnClickListener(view -> {
            holder.progress.setVisibility(View.VISIBLE);
            holder.buy.setEnabled(false);
            buyClickListener.onClick(item.getId());
        });
    }

    public static class ViewHolder extends BaseViewHolder<StoreItem> {

        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.amount)
        TextView amount;
        @BindView(R.id.buy)
        Button buy;
        @BindView(R.id.progress)
        ProgressBar progress;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
