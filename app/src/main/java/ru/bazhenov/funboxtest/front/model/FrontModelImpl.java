package ru.bazhenov.funboxtest.front.model;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import rx.Observable;

import static ru.bazhenov.funboxtest.Constants.DELAY_BUY_ITEM;

public class FrontModelImpl implements FrontModel {

    @Override
    public Observable<RealmResults<StoreItem>> getStoreItems() {
        return Realm.getDefaultInstance().where(StoreItem.class)
                .notEqualTo("amount", 0)
                .findAll()
                .asObservable();
    }

    @Override
    public void updateStoreItem(long itemId, Realm.Transaction.OnError onError) {
        Realm.getDefaultInstance().executeTransactionAsync(
                realm -> {
                    waitThreeSeconds();
                    StoreItem item = getStoreItem(itemId, realm);
                    item.setAmount(item.getAmount() - 1);
                },
                onError);
    }

    private StoreItem getStoreItem(long itemId, Realm realm) {
        return realm.where(StoreItem.class).equalTo("id", itemId).findFirst();
    }

    private void waitThreeSeconds() {
        try {
            Thread.sleep(DELAY_BUY_ITEM);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
