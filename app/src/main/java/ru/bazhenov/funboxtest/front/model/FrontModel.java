package ru.bazhenov.funboxtest.front.model;

import io.realm.Realm;
import io.realm.RealmResults;
import ru.bazhenov.funboxtest.data_providers.StoreItem;
import rx.Observable;

public interface FrontModel {

    Observable<RealmResults<StoreItem>> getStoreItems();

    void updateStoreItem(long itemId, Realm.Transaction.OnError onError);
}
